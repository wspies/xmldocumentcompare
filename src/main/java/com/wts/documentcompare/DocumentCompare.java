package com.wts.documentcompare;

import com.sun.org.apache.xerces.internal.impl.xs.util.StringListImpl;
import com.sun.org.apache.xerces.internal.xs.StringList;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;

import java.util.ArrayList;
import java.util.List;

public class DocumentCompare {

    private String compareSource;
    private String compareResult;
    private String ignoreNodeList;
    private IgnoreNodes ignoreNodes;
    private Diff diff;


    public DocumentCompare(String compareSource, String compareResult) {
        setCompareSource(compareSource);
        setCompareResult(compareResult);
    }

    public DocumentCompare(String compareSource, String compareResult,String ignoreNodeList) {
        setCompareSource(compareSource);
        setCompareResult(compareResult);
        setIgnoreNodeList(ignoreNodeList);

        this.ignoreNodes = new IgnoreNodes(getIgnoreNodesList());
    }

    public boolean isMatch(){

        boolean match;

        match();

        match = this.diff.hasDifferences();

        return !match;
    }

    public String getDifferences(){

        List<String> differences = new ArrayList<String>();

        match();

        for (Difference diff : this.diff.getDifferences()) {
            differences.add(diff.toString());
            System.out.println(diff);
        }

        return differences.toString();
    }

    private void match(){

        this.diff = DiffBuilder.compare(getCompareSource())
                .withTest(getCompareResult())
                .withNodeFilter(this.ignoreNodes)
                .ignoreWhitespace()
                .checkForSimilar()
                .build();
    }

    public void setCompareSource(String compareSource) {
        this.compareSource = compareSource;
    }

    public void setCompareResult(String compareResult) {
        this.compareResult = compareResult;
    }

    public String getCompareSource() {
        return this.compareSource;
    }

    public String getCompareResult() {
        return this.compareResult;
    }

    public void setIgnoreNodeList(String ignoreNodes) {
        this.ignoreNodeList = ignoreNodes;
    }

    public String getIgnoreNodesList() {
        return ignoreNodeList;
    }
}
