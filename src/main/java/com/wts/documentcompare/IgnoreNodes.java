package com.wts.documentcompare;

import org.w3c.dom.Node;
import org.xmlunit.util.Predicate;

class IgnoreNodes implements Predicate<Node> {

    private String nodeList;

    public IgnoreNodes(String nodeList) {
        this.nodeList = nodeList;
    }

    public boolean test(Node node) {

        //System.out.println(node.getNodeName() + ":" + !(nodeList.contains(node.getNodeName())));
        // check if the nodeList to ignore contains the current nodename, if so, do not compare
        return !(nodeList.contains(node.getNodeName()));
    }

    public void setNodeList(String nodeList) {
        this.nodeList = nodeList;
    }
}